terraform {
    backend "s3" {
        bucket = "mastodon-bucket-s3"
        key = "terraform/mastodon-key"
        region = "eu-central-1"
    }
}

provider "aws" {
    region = "eu-central-1"
    profile = "emperor-of-sand"
}

resource "aws_instance" "mastodon" {
    depends_on = [
        "aws_security_group.allow_http_inbound_sg",
        "aws_security_group.allow_team_sg"

    ]

    ami = "ami-1e339e71"
    instance_type = "t2.micro"
    key_name="iarama"

    vpc_security_group_ids =
        [ "${aws_security_group.allow_http_inbound_sg.id}", "${aws_security_group.allow_team_sg.id}" ]

    tags {
        Name = "tag_mastodon"
        }

    user_data = "${file("manb3ast_data.sh")}"

    provisioner "file" {
      source      = "app"
      destination = "/home/ubuntu"

      connection {
        type     = "ssh"
        user     = "ubuntu"
        private_key = "${file("~/.ssh/id_rsa")}"
            }
        }
}

resource "aws_lb_target_group" "mastodon-target-group" {
    port = 9090
    protocol = "HTTP"
    vpc_id = "vpc-201d0e48"

    health_check {
        healthy_threshold   = 2
        unhealthy_threshold = 2
        timeout             = "10"
        port                = "9090"
        path                = "/"
        protocol            = "HTTP"
        interval            = 30
        matcher             = "200-399"
    }

    tags {
        application = "mastodon"
        name = "mastodon-target-group"
    }
}

resource "aws_lb_target_group_attachment" "mastodon-attachment-group" {
    target_group_arn = "${aws_lb_target_group.mastodon-target-group.arn}"
    target_id = "${aws_instance.mastodon.id}"
    port = 9090
}

resource "aws_security_group" "allow_alb" {
    name = "alb_mastodon_sg"

    ingress {
        from_port = 9090
        to_port = 9090
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_security_group" "allow_http_inbound_sg" {
    name = "allow_http_sg"

    ingress {
        from_port = 9090
        to_port = 9090
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        security_groups = ["${aws_security_group.allow_alb.id}"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_security_group" "allow_team_sg" {
    name = "accessness_sg"

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["109.166.194.99/32"]
    }
}

resource "aws_lb" "mastodon-alb" {
    name            = "mastodon-alb"
    internal        = true
    security_groups = ["${aws_security_group.allow_http_inbound_sg.id}"]
    subnets = [
        "subnet-3f5ebb42",
        "subnet-75538338",
        "subnet-d88721b3"]
    enable_deletion_protection = false

    tags {
        Name = "mastodon-alb"
        application = "mastodon"
    }
}

resource "aws_alb_listener" "mastodon-alb-listener" {
    "default_action" {
        target_group_arn = "${aws_lb_target_group.mastodon-target-group.arn}"
        type = "forward"
    }

    load_balancer_arn = "${aws_lb.mastodon-alb.arn}"
    port = 9090
    protocol = "HTTP"
}

resource "aws_route53_zone" "mastodon-ddigital" {
    name = "mastodon.ddigital.org."
}

resource "aws_route53_record" "mastodon-route53" {
    name = "mastodon-route53.mastodon.ddigital.org."
    type = "A"
    ttl = "300"
    zone_id = "${aws_route53_zone.mastodon-ddigital.zone_id}"
    records = ["${aws_instance.mastodon.public_ip}"]
}

resource "aws_route53_record" "mastodon-ns" {
    name = "mastodon.ddigital.org."
    type = "NS"
    ttl = "300"
    zone_id = "Z1QKN3QUNQFAR"
    records = ["${aws_route53_zone.mastodon-ddigital.name_servers}"]
}

resource "aws_route53_record" "mastodon-srv-tcp" {
    name = "mastodon-srv-tcp.mastodon.ddigital.org."
    type = "SRV"
    ttl = "300"
    zone_id = "${aws_route53_zone.mastodon-ddigital.zone_id}"
    records = ["1 1 9090 ${aws_instance.mastodon.public_dns}"]
}

output "dns_route53" {
    value = "${aws_route53_zone.mastodon-ddigital.name}"
}

output "dnsA" {
    value = "${aws_route53_record.mastodon-route53.fqdn}"
}

output "dnsSRV" {
    value = "${aws_route53_record.mastodon-srv-tcp.fqdn}"
}

output "server_ip" {
    value = "${aws_instance.mastodon.public_ip}"
}

output "dns_name" {
    value = "${aws_instance.mastodon.public_dns}"
}